package de.tudresden.inf.st.jastadd.grammar2uml.test;

import org.jastadd.relast.compiler.CompilerException;
import de.tudresden.inf.st.jastadd.grammar2uml.compiler.Compiler;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * TODO: Add description.
 *
 * @author rschoene - Initial contribution
 */
public class CompilerTest {

  @SuppressWarnings("SameParameterValue")
  void transform(String inputGrammar, String inputGrammar2Uml, String output) throws CompilerException {

    System.out.println("Running test in directory '" + Paths.get(".").toAbsolutePath() + "'.");
    assertTrue(Paths.get(inputGrammar).toFile().exists(), "input grammar does not exist");
    assertTrue(Paths.get(inputGrammar2Uml).toFile().exists(), "input grammar2uml does not exist");
    assertFalse(Paths.get(inputGrammar).toFile().isDirectory(), "input grammar is a directory");
    assertFalse(Paths.get(inputGrammar2Uml).toFile().isDirectory(), "input grammar2uml is a directory");

    File outputFile = Paths.get(output).toFile();
    File outputDirFile = outputFile.getParentFile();
    if (!outputDirFile.exists()) {
      assertTrue(outputDirFile.mkdir());
    }

    String[] args = {
        "--output=" + output,
        "--inputGrammar2Uml=" + inputGrammar2Uml,
        "--defaultFolders",
        inputGrammar
    };

    new Compiler().run(args);
  }

  @Test
  void transformMinimalExample() throws CompilerException {
    transform("src/test/resources/Example.relast", "src/test/resources/Example.grammar2uml", "src/test/resources/uml.md");
  }
}
