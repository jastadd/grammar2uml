package de.tudresden.inf.st.jastadd.grammar2uml.compiler;

import beaver.Parser;
import de.tudresden.inf.st.jastadd.grammar2uml.ast.*;
import de.tudresden.inf.st.jastadd.grammar2uml.parser.Grammar2UmlParser;
import de.tudresden.inf.st.jastadd.grammar2uml.scanner.Grammar2UmlScanner;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Testing Grammar2Uml without parser.
 *
 * @author rschoene - Initial contribution
 */
public class Grammar2UmlMain {

  public static void main(String[] args) {
//    testing();
    processManualAST();
  }

  public static Grammar2Uml createManualAST() {
    System.setProperty("mustache.debug", "true");
    Grammar2Uml model = new Grammar2Uml();
    Path path = Paths.get("grammar2uml.base", "src", "test", "resources", "Example.relast");
    System.out.println("path.toFile().getAbsolutePath() = " + path.toFile().getAbsolutePath());
    Program program = parseProgram(path);
    model.setProgram(program);

    Folder folder1 = new Folder();
    folder1.setName("Folder1");
    folder1.addType(program.resolveTypeDecl("Wert"));
    folder1.addType(program.resolveTypeDecl("Quelle"));
    model.addFolder(folder1);

    return model;
  }

  private static void processManualAST() {
    Grammar2Uml model = createManualAST();
    model.treeResolveAll();
    for (Folder f : model.getFolderList()) {
      System.out.println(f.getName() + ":");
      for (TypeDecl typeDecl : f.getTypeList()) {
        System.out.println(typeDecl.getName());
      }
    }

    System.out.println(model.generateAspect());
  }

  public static Program parseProgram(Path path) {
    try (BufferedReader reader = Files.newBufferedReader(path)) {
      Grammar2UmlScanner scanner = new Grammar2UmlScanner(reader);
      Grammar2UmlParser parser = new Grammar2UmlParser();
      GrammarFile grammarFile = (GrammarFile) parser.parse(scanner);
      Program program = new Program();
      program.addGrammarFile(grammarFile);
      return program;
    } catch (IOException | Parser.Exception e) {
      e.printStackTrace();
    }
    return null;
  }
}
