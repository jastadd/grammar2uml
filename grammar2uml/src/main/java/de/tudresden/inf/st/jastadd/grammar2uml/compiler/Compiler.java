package de.tudresden.inf.st.jastadd.grammar2uml.compiler;

import beaver.Parser;
import de.tudresden.inf.st.jastadd.grammar2uml.ast.ErrorMessage;
import de.tudresden.inf.st.jastadd.grammar2uml.ast.Grammar2Uml;
import de.tudresden.inf.st.jastadd.grammar2uml.ast.GrammarFile;
import de.tudresden.inf.st.jastadd.grammar2uml.ast.Program;
import de.tudresden.inf.st.jastadd.grammar2uml.parser.Grammar2UmlParser;
import de.tudresden.inf.st.jastadd.grammar2uml.scanner.Grammar2UmlScanner;
import net.sourceforge.plantuml.FileFormat;
import net.sourceforge.plantuml.FileFormatOption;
import net.sourceforge.plantuml.FileUtils;
import net.sourceforge.plantuml.SourceStringReader;
import org.jastadd.option.BooleanOption;
import org.jastadd.option.ValueOption;
import org.jastadd.relast.compiler.AbstractCompiler;
import org.jastadd.relast.compiler.CompilerException;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class Compiler extends AbstractCompiler {

  private ValueOption optionOutputFile;
  private ValueOption optionInputGrammar2Uml;
  private BooleanOption optionDefaultFolders;
  private BooleanOption optionHelp;
  private BooleanOption optionVersion;
  private BooleanOption optionVerbose;

  public Compiler() {
    super("grammar2uml" , false);
  }

  /**
   * Reads the version string.
   * <p>
   * The version string is read from the property file
   * src/main/resources/Version.properties. This
   * file should be generated during the build process. If it is missing
   * then there is some problem in the build script.
   *
   * @return the read version string, or <code>version ?</code>
   * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
   */
  private String readVersion() {
    try {
      return ResourceBundle.getBundle("grammar2umlVersion").getString("version");
    } catch (MissingResourceException e) {
      return "version ?";
    }
  }

  public static void main(String[] args) {
    System.setProperty("mustache.debug" , "true");
    try {
      new Compiler().run(args);
    } catch (CompilerException e) {
      System.err.println(e.getMessage());
      System.exit(-1);
    }
  }

  private void printMessage(String message) {
    System.out.println(message);
  }

  protected void initOptions() {
    optionOutputFile = addOption(
        new ValueOption("output" , "target file to be generated.")
            .defaultValue("uml.png")
            .acceptAnyValue()
            .needsValue(false));
    optionInputGrammar2Uml = addOption(
        new ValueOption("inputGrammar2Uml" , "grammar2uml definition file.")
            .needsValue(true));
    optionDefaultFolders = addOption(
        new BooleanOption("defaultFolders" ,
            "Creates a default folder per grammar file.")
            .defaultValue(false));
    optionHelp = addOption(
        new BooleanOption("help" , "Print usage and exit.")
            .defaultValue(false));
    optionVersion = addOption(
        new BooleanOption("version" , "Print version and exit.")
            .defaultValue(false));
    optionVerbose = addOption(
        new BooleanOption("verbose" , "Print more messages while compiling.")
            .defaultValue(false));
  }

  private Grammar2Uml parseProgram() throws CompilerException {
    Program program = new Program();
    Grammar2Uml grammar2Uml;

    for (String inputGrammarFileName : getConfiguration().getFiles()) {
      printMessage("Parsing " + inputGrammarFileName);
      GrammarFile inputGrammar;
      try (BufferedReader reader = Files.newBufferedReader(Paths.get(inputGrammarFileName))) {
        Grammar2UmlScanner scanner = new Grammar2UmlScanner(reader);
        Grammar2UmlParser parser = new Grammar2UmlParser();
        inputGrammar = (GrammarFile) parser.parse(scanner);
        if (optionVerbose.value()) {
          inputGrammar.dumpTree(System.out);
        }
        program.addGrammarFile(inputGrammar);
        inputGrammar.setFileName(inputGrammarFileName);
      } catch (IOException | Parser.Exception e) {
        throw new CompilerException("Could not parse grammar file " + inputGrammarFileName, e);
      }
    }

    if (optionInputGrammar2Uml.isMatched()) {
      String inputGrammar2UmlFileName = optionInputGrammar2Uml.value();
      try (BufferedReader reader = Files.newBufferedReader(Paths.get(inputGrammar2UmlFileName))) {
        Grammar2UmlScanner scanner = new Grammar2UmlScanner(reader);
        Grammar2UmlParser parser = new Grammar2UmlParser();
        grammar2Uml = (Grammar2Uml) parser.parse(scanner, Grammar2UmlParser.AltGoals.grammar2uml);
        grammar2Uml.setFileName(inputGrammar2UmlFileName);
      } catch (IOException | Parser.Exception e) {
        throw new CompilerException("Could not parse grammar2uml file " + inputGrammar2UmlFileName, e);
      }
    } else {
      // no special setting given
      grammar2Uml = new Grammar2Uml();
      grammar2Uml.setFileName("<none>");
    }
    grammar2Uml.setProgram(program);
    grammar2Uml.treeResolveAll();
    if (optionDefaultFolders.value()) {
      for (GrammarFile grammarFile : program.getGrammarFileList()) {
        grammar2Uml.addFolder(grammarFile.defaultFolder());
      }
    }
    return grammar2Uml;
  }

  @Override
  protected int compile() throws CompilerException {
    if (optionVersion.value()) {
      System.out.println(readVersion());
      return 0;
    }
    if (optionHelp.value()) {
      getConfiguration().printHelp(System.out);
      return 0;
    }

    printMessage("Running grammar2uml " + readVersion());

    Path destination = getDestinationPath();
    Path parent = Paths.get(optionOutputFile.value()).toAbsolutePath().getParent();
    try {
      Files.createDirectories(parent);
    } catch (IOException e) {
      throw new CompilerException("Error creating output dir " + parent, e);
    }

    if (getConfiguration().getFiles().isEmpty()) {
      throw new CompilerException("No input grammars specified!");
    }

    Grammar2Uml grammar2uml = parseProgram();

    if (!grammar2uml.errors().isEmpty()) {
      System.err.println("Errors:");
      for (ErrorMessage e : grammar2uml.errors()) {
        System.err.println(e);
      }
      System.exit(1);
    }

    printMessage("Writing output file " + destination);

    String sourceCode = grammar2uml.generateAspect();

    //
    String extension = fileExtensionOf(destination).toUpperCase();
    FileFormatOption plantUmlOption = null;
    switch (extension) {
      case "MD":
        try {
          Files.writeString(destination, sourceCode);
        } catch (Exception e) {
          throw new CompilerException("Could not write to file " + destination, e);
        }
        break;
      case "HTML":
      case "PNG":
      case "PDF":
      case "SVG":
        plantUmlOption = new FileFormatOption(FileFormat.valueOf(extension));
        break;
    }
    if (plantUmlOption != null) {
      try {
        SourceStringReader reader = new SourceStringReader(sourceCode);
        reader.outputImage(Files.newOutputStream(destination), plantUmlOption);
      } catch (Exception e) {
        throw new CompilerException("Could not write to file " + destination, e);
      }
    }

    return 0;
  }

  private String fileExtensionOf(Path path) {
    String fileName = path.toFile().getName();
    int lastIndexOfDot = fileName.lastIndexOf(".");
    return fileName.substring(lastIndexOfDot + 1);
  }

  private Path getDestinationPath() {
    return Paths.get(optionOutputFile.value()).toAbsolutePath();
  }
}
