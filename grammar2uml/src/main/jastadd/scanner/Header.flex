package de.tudresden.inf.st.jastadd.grammar2uml.scanner;

import de.tudresden.inf.st.jastadd.grammar2uml.parser.Grammar2UmlParser.Terminals;
%%

%public
%final
%class Grammar2UmlScanner
%extends beaver.Scanner

%type beaver.Symbol
%function nextToken
%yylexthrow beaver.Scanner.Exception
%scanerror Grammar2UmlScanner.ScannerError

%x COMMENT
%s DECLARATION

%line
%column
