# Grammar2Uml

The tool called `Grammar2Uml` ([see in repo](https://git-st.inf.tu-dresden.de/jastadd/grammar2uml)) takes a set of grammar specifications and creates a visualization similar to UML class diagrams.

![](img/grammar2uml.png)
![](img/minimal-example.png)

Every nonterminal will be shown as a rectangular box with its terminal children listed within it. Children are shown as containment relations between parent and child, whereas relations and intrinsic references are shown using arrows.

Furthermore, more options may be specified.

|         Name         |                               Description                               |
|----------------------|-------------------------------------------------------------------------|
| `--output`           | target file to be generated (default: `uml.md` or `uml.png`, depending on `--printSource`) |
| `--inputGrammar2Uml` | grammar2uml definition file, see [below](#grammar2uml-definition-file). |
| `--printSource`      | If set, the output will be the source code instead of a png. (default: create a png) |
| `--defaultFolders`   | If set, creates a default folder per grammar file. (default: use one global folder) |
| `--help`             | Print usage and exit.                                                   |
| `--version`          | Print version and exit.                                                 |
| `--verbose`          | Print more messages while compiling.                                    |

## Grammar2uml definition file

To structure the generated visualization, any number of nonterminals can be grouped with folders.
Either specify the option `--defaultFolders` to use one folder per input grammar file, or specify a grammar2uml definition file.
The definition file has a simple syntax, it is a list of folder specifications. One specifications looks like

```
folder FOLDER_NAME : NT1, NT2, ..., NT_n
```

It produces one folder named `FOLDER_NAME` containing all nonterminals (`NT1`, `NT2`, ..., `NT_n`) following it.
