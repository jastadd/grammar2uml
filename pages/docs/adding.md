# Add grammar2uml to your project

To use `Grammar2uml`, adjust your `build.gradle` as follows.

Set up the maven package source as repository:

```gradle
repositories {
    mavenCentral()
    maven {
        name 'gitlab-maven'
        url 'https://git-st.inf.tu-dresden.de/api/v4/groups/jastadd/-/packages/maven'
    }
}
```

Add `Grammar2uml` as a dependency:

```
configurations {
    grammar2uml
}

dependencies {
    grammar2uml group: 'de.tudresden.inf.st', name: 'grammar2uml', version: '0.2.2'
}
```

Add a task to create your visualization:

```
task grammar2uml(type: JavaExec) {
    group = 'Documentation'
    classpath = configurations.grammar2uml

    args '--output=YourOutput.png', '--defaultFolders'
    args 'src/main/jastadd/YourGrammar.relast'
}
```

## Build from source (not recommended)

If you want to build `Grammar2Uml` from source, first build the fat-jar from the [repository](https://git-st.inf.tu-dresden.de/jastadd/grammar2uml).
The normal jar does not suffice, as it lacks the information on needed dependencies.

```bash
git clone https://git-st.inf.tu-dresden.de/jastadd/grammar2uml.git
cd grammar2uml
./gradlew fatJar
ls grammar2uml/build/libs/
```

This jar can then be copied to your project.

```bash
cp grammar2uml/build/libs/grammar2uml-fatjar-<version>.jar ${YourProject}/libs/grammar2uml.jar
```

Finally, this jar has to be integrated into your build process. In case [Gradle](https://gradle.org/) is used, the jar file needs to be added as dependency using:

```groovy
dependencies {
    grammar2uml fileTree(include: ['grammar2uml.jar'], dir: 'libs')
}
```

The path to the jar file may need to be changed according to your project structure.
