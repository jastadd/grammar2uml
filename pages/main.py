import os

grammar2umlVersionFileName = '../grammar2uml/src/main/resources/grammar2umlVersion.properties'


def get_version():
    with open(grammar2umlVersionFileName) as grammar2umlVersionFile:
        versionFileContent = grammar2umlVersionFile.read()
    return versionFileContent[versionFileContent.rindex('version=') + 8:].strip()


def define_env(env):
    """
    This is the hook for defining variables, macros and filters

    - variables: the dictionary that contains the environment variables
    - macro: a decorator function, to declare a macro.
    """
    env.conf['site_name'] = 'grammar2uml ' + get_version()

    @env.macro
    def grammar2uml_version():
        return get_version()


if __name__ == '__main__':
    print(get_version())
